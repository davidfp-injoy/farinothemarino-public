using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpriteSet
{
    [HideInInspector]
    public GameObject current;

    [HideInInspector]
    public GameObject previous;

    [HideInInspector]
    public Animator animator;

    protected void UpdateSprite(GameObject spriteObject)
    {
        previous = current;
        current = spriteObject;
        animator = current.GetComponent<Animator>();
        
        if (previous != null)
            previous.SetActive(false);
        
        current.SetActive(true);
    }

    public abstract void SetByName<T>(T spriteName);
}
