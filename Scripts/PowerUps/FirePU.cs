using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePU : PowerUp
{
    private GameObject fireballObject;
    // private float fireballLifespan = 0;

    public override void Action()
    {   
        if (fireballObject == null)
            AdditionalSetup();

        GameObject instance = GameObject.Instantiate(fireballObject);
        
        Fireball fireball = instance.GetComponent<Fireball>();

        string shooterLookingAt = fireball.shooterIostate.isLookingAt;
        Vector2 shooterPosition = fireball.shooterObject.transform.position;

        fireball.transform.position = new Vector2(
            shooterPosition.x,
            shooterPosition.y + 1f
        );

        fireball.SetLookingDir(shooterLookingAt);

        fireball.DisableRigidbodySim(false);

        Debug.Log("Fireeeeoooouooooo!!!");
    }

    protected override void AdditionalSetup()
    {
        fireballObject = GameObject.Find("Fireball");
        
        if (fireballObject == null) return;

        Fireball fireball = fireballObject.GetComponent<Fireball>();

        fireball.lifespan = 5;
    }

}