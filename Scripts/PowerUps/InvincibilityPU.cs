using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityPU : PowerUp
{
    protected override void AdditionalSetup()
    {
        AudioManager bgmMan = Camera.main.GetComponent<AudioManager>();
        bgmMan.Stop();
        bgmMan.Play("Invincible OST");
        
        GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
        Player player = playerObj.GetComponent<Player>();
        player.playerSprites.SetInvincible();
    }
}