﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpItem : Item
{
    protected PowerUpData powerUpData;


    void Update()
    {
        gameObject.tag = "PowerUp";
    }


    public override void Awake()
    {
        powerUpData = GetComponent<PowerUpData>();
        base.Awake();
    }


    void OnCollisionEnter2D (Collision2D collision)
    {
        return;
    }

    override protected void TriggerAction()
    {
        if (playerStats.currentPowerUp.priority > powerUpData.priority)
            return;

        playerStats.savedPowerUp = playerStats.currentPowerUp;
        playerStats.currentPowerUp = CreatePowerUp(powerUpData);
    }


    private PowerUp CreatePowerUp(PowerUpData data)
    {
        Type type = Type.GetType(data.type);

        PowerUp powerUp = Activator.CreateInstance(type) as PowerUp;

        powerUp.SetData(
            data.sprite,
            data.duration,
            data.getsGrown,
            data.getsImmune,
            data.priority,
            data.actionCooldown,
            data.getsUntouchable
        );

        return powerUp;
    }
}
