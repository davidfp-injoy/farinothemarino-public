using UnityEngine;

public class PowerUp
{
    public float duration = 0; // 0 = infinito
    public SpriteName sprite;
    public bool getsGrown = true;
    public bool getsImmune = false;
    public bool getsUntouchable = false;
    public bool isSet = false;
    public int priority = 1;
    public float actionCooldown = 0.5f;
    public bool canUseAction = true;

    public void SetData(
        SpriteName _sprite,
        float _duration = 0,
        bool _getsGrown = true,
        bool _getsImmune = true,
        int _priority = 1,
        float _actionCooldown = 0.5f,
        bool _getsUntouchable = false
    )
    {
        duration        =  _duration;
        sprite          =  _sprite;
        getsGrown       =  _getsGrown;
        getsImmune      =  _getsImmune;
        priority        =  _priority;
        actionCooldown  =  _actionCooldown;
        isSet           =  true;
        getsUntouchable =  _getsUntouchable;
        AdditionalSetup();
    }

    public virtual void Action() { }

    protected virtual void AdditionalSetup() { }
}