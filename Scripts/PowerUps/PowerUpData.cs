using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpData : MonoBehaviour
{
    public string type;
    public float duration = 0; // 0 = infinito
    public SpriteName sprite;
    public bool getsGrown = true;
    public bool getsImmune = false;
    public bool getsUntouchable = false;
    public int priority = 1;
    public float actionCooldown = 0.5f;
}