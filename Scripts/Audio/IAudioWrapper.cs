using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAudioWrapper {
    AudioClip clip { get; set; }
    string audioName { get; set; }
}