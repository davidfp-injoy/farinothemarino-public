using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioWrapper {
    public AudioClip clip;
    public string audioName;

    public AudioWrapper(AudioClip clip = null, string audioName = "")
    {
        this.clip = clip;
        this.audioName = audioName;
    }
}