using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AudioWrapperStruc {
    public AudioClip clip;
    public string audioName;
}