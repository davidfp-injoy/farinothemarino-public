﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioManager : MonoBehaviour
{
    public AudioWrapperStruc[] audios;

    [HideInInspector]
    public AudioSource controlAudio;
    [HideInInspector]
    public string currentAudioName;

    void Awake()
    {
        controlAudio = GetComponent<AudioSource>();
    }

    public void Play(string audioName, bool loop = false, float volume = 1)
    {
        if (controlAudio == null)
            controlAudio = GetComponent<AudioSource>();

        if (controlAudio == null || audioName == "") return;

        AudioWrapper audio = Find(audioName);
        if (audio.clip == null) return;

        currentAudioName = audioName;
        controlAudio.clip = audio.clip;
        controlAudio.PlayOneShot(audio.clip, volume);
    }


    public void Replay()
    {
        if (controlAudio == null)
            controlAudio = GetComponent<AudioSource>();

        if (controlAudio == null) return;
        controlAudio.Play();
    }


    public void Stop()
    {
        if (controlAudio == null)
            controlAudio = GetComponent<AudioSource>();
    
        if (controlAudio == null) return;
        controlAudio.Stop();
    }


    private AudioWrapper Find(string audioName)
    {
        AudioWrapper foundAudio = new AudioWrapper();

        foreach (AudioWrapperStruc audio in audios) {
            if (audio.audioName != audioName) continue;
            foundAudio = new AudioWrapper(audio.clip, audio.audioName);
        }

        return foundAudio;
    }
}
