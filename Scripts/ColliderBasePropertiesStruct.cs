using UnityEngine;

[System.Serializable]
public struct ColliderBasePropertiesStruct {
    public Vector2 size;
    public Vector2 offset;
    public bool showGizmo;
}