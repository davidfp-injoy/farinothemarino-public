﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractiveObject : MonoBehaviour
{
    public enum DispatchType { Stay, Deactivate, Destroy };
    public enum CollidersType { Box, Capsule, Circle, Composite, Edge, Polygon, Tilemap };

    public float lifespan = 0; // “0” sería infinito
    public bool startInactive = true;
    public GameObject sprite;
    public CollidersType colliderType = CollidersType.Box;
    public DispatchType dispatchType = DispatchType.Destroy;
    public float dispatchTime = 0;
    public bool dispatchCollider = true;
    public bool mirrorSprite = true;
    public string initLookingDirection;

    [HideInInspector]
    public AudioManager audioMan;
    [HideInInspector]
    public AudioManager bgmMan;
    [HideInInspector]
    public float spriteInitScale;

    [HideInInspector]
    public string type; // Debe estar siempre en minúsculas

    [HideInInspector]
    public bool isDispatched = false;

    [HideInInspector]
    public Rigidbody2D _rigidbody;

    [HideInInspector]
    public IOState iostate;

    protected Animator animator;

    // COLLIDER

    [HideInInspector]
    public Collider2D mainCollider;



    // Coroutines
    protected Coroutine C_Dispatch;
    protected Coroutine C_RunLifeTime;


    public virtual void Awake()
    {
        _rigidbody      = gameObject.GetComponent<Rigidbody2D>();
        type            = gameObject.GetComponent<ObjectType>().type;
        iostate         = gameObject.GetComponent<IOState>();
        animator        = sprite.GetComponent<Animator>();
        spriteInitScale = sprite.transform.localScale.x;

        SetSFXManager();
        SetBGMManager();
        SetMainCollider();

        if (lifespan > 0)
            C_RunLifeTime = StartCoroutine("RunLifeTime");

        if (iostate != null)
            iostate.isLookingAt = initLookingDirection;
    }

    public virtual void Start() {}

    
    protected virtual IEnumerator Dispatch()
    {
        yield return new WaitForSeconds(dispatchTime);
        
        if (dispatchType == DispatchType.Deactivate)
            gameObject.SetActive(false);
        else if (dispatchType == DispatchType.Destroy)
            Destroy(gameObject);        
    }


    public virtual void RunDispatch()
    {
        isDispatched = true;
        
        PlayDispatchAnim();
        
        if (dispatchCollider)
            DisableCollider();

        C_Dispatch = StartCoroutine("Dispatch");
    }


    protected virtual void PlayDispatchAnim()
    {
        PlayAnim("isDispatched");
    }


    public virtual void PlayAnim(string param, bool value = true)
    {
        if (animator == null) return;

        animator.SetBool(param, value);
    }


    protected virtual void SetMainCollider()
    {
        string[] colliderTypesList = {
            "Box", "Capsule", "Circle", 
            "Composite", "Edge", "Polygon", 
            "Tilemap"
        };

        string componentName = colliderTypesList[(int)colliderType] + "Collider2D";

        mainCollider = GetComponent(componentName) as Collider2D;
    }

    
    protected virtual IEnumerator RunLifeTime()
    {
        yield return new WaitForSeconds(lifespan);
        dispatchTime = 0;
        RunDispatch();
    }


    public void SetLookingDir(string direction)
    {
        if (iostate != null)
            iostate.currentDirection = direction;

        bool noDirection = direction != "left" && direction != "right";

        if (noDirection) {
            if (iostate != null)
                iostate.currentDirection = null;
    
            return;
        } else {
            if (iostate != null)
                iostate.isLookingAt = direction;
        }

        if (!mirrorSprite) return;

        float x, y, z;
        x = y = z = spriteInitScale;
        x = direction == "right" ? x : -x;

        sprite.transform.localScale = new Vector3(x, y, z);
    }


    public void DisableCollider(bool disable = true)
    {
        mainCollider.enabled = !disable;
    }

    public void DisableRigidbodySim(bool disable = true)
    {
        _rigidbody.simulated = !disable;
    }


    protected void SetSFXManager(string tag = "Audio Manager")
    {
        GameObject obj = GameObject.FindGameObjectWithTag(tag);
        if (obj == null) return;
        audioMan = obj.GetComponent<AudioManager>();
    }


    public void PlaySound(string audioName, bool loop = false, float volume = 1)
    {
        if (audioMan == null) return;
        audioMan.Play(audioName, loop, volume);
    }

    public void StopSound()
    {
        if (audioMan == null) return;
        audioMan.Stop();
    }

    protected void SetBGMManager()
    {
        bgmMan = Camera.main.GetComponent<AudioManager>();
    }


    public void PlayBGM(string audioName, bool loop = false, float volume = 1)
    {
        if (bgmMan == null) return;
        bgmMan.Play(audioName, loop, volume);
    }

    public void StopBGM()
    {
        if (bgmMan == null) return;
        bgmMan.Stop();
    }
}
