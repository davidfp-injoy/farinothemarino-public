﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroying : MonoBehaviour
{
    public float destroyTime = 0; 

    void Start()
    {
        if (destroyTime > 0)
            StartCoroutine("RunDestroy");
    }


    private IEnumerator RunDestroy()
    {
        yield return new WaitForSeconds(destroyTime);
        SuddenDestroy();
    }


    public void SuddenDestroy()
    {
        Destroy(gameObject);
    }
}
