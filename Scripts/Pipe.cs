﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : TriggerObject
{
    [Header("Pipe options")]
    public GameObject destinationPipeObject ;
    protected Pipe destinationPipe;
    public DirectionsEnum exitDirection = DirectionsEnum.Up;
    public bool closeAfterUse = true;
    public bool hasEnemy = false;
    public GameObject enemyPrefab;
    public GameObject fixCameraOn;
    public string destinationBGMAudioName;
    public float travelerPositionZ = 2.3f;
    private float previousTravelerPositionZ = 0;
    private GameObject enemyInstance;




    protected bool alreadyUsed = false;
    protected bool reachedDestination;
    [HideInInspector]
    public bool isHorizontal;

    // CHILDREN
    [HideInInspector]
    public GameObject exit; 
    [HideInInspector]
    public GameObject centerMarker;
    [HideInInspector]
    protected float travelerAnimationSpeed = 0.1f;


    // CHARACTER
    protected GameObject travelerObject;
    protected Character travelerCharacter;
    protected IOState travelerIOState; 


    // COROUTINES
    private Coroutine C_Align;
    private Coroutine C_AnimateTravel;

    public override void Awake()
    {
        base.Awake();

        exit          =  gameObject.transform.Find("Exit").gameObject;
        centerMarker  =  gameObject.transform.Find("CenterMarker").gameObject;

        isHorizontal  =  exitDirection == DirectionsEnum.Left || exitDirection == DirectionsEnum.Right;

        if (destinationPipeObject != null)
            destinationPipe = destinationPipeObject.GetComponent<Pipe>();

        // SetChildrenPositions();

        InstantiateEnemy();
    }


    protected void FixedUpdate()
    {
        DisableEnemy();
    }


    public override void  Trigger()
     {
        travelerCharacter.StopControls();
        SetCharacterState();


        if (isHorizontal)
            C_Align = StartCoroutine("AlignVertical");
        else
            C_Align = StartCoroutine("AlignHorizontal");

        alreadyUsed = closeAfterUse;
    }



    public void Travel(GameObject traveler, bool conditionsMet)
    {
        SetTraveler(traveler);

        if (!destinationPipeObject 
            || alreadyUsed 
            || !conditionsMet 
            || travelerIOState.isTraveling
        )
            return;

        Trigger();
    }



    private void SetTraveler(GameObject traveler)
    {
        travelerObject    =  traveler;
        travelerCharacter =  travelerObject.GetComponent<Character>();
        travelerIOState   =  travelerObject.GetComponent<IOState>();
    }



    protected void SetCharacterState()
    {
        travelerIOState.ResetState();
        travelerIOState.isImmune    = true;
        travelerIOState.isTraveling = true;
        
        travelerCharacter.StopControls();
        travelerCharacter.DisableRigidbodySim();
        travelerCharacter.DisableCollider();
    }



    protected void ResetCharacterState()
    {
        travelerIOState.ResetState();
        
        travelerCharacter.StopControls(false);
        travelerCharacter.DisableRigidbodySim(false);
        travelerCharacter.DisableCollider(false);

        SetTravelerOutPosition();
    }



    protected IEnumerator AlignHorizontal()
    {
        bool isAligned = false;
        bool fromRightSide = travelerObject.transform.position.x > gameObject.transform.position.x;
        float movementSpeed = fromRightSide ? -travelerAnimationSpeed : travelerAnimationSpeed;

        centerMarker.transform.position = new Vector2(
            gameObject.transform.position.x,
            travelerObject.transform.position.y
        );

        while (!isAligned) {
            yield return new WaitForFixedUpdate();

            float distance = Vector2.Distance(
                travelerObject.transform.position, 
                centerMarker.transform.position
            );

            if (distance <= travelerAnimationSpeed) {
                isAligned = true;
                movementSpeed = 0;
                travelerObject.transform.position = centerMarker.transform.position;
            }

            travelerObject.transform.Translate(new Vector2(movementSpeed, 0));
        }

        AnimateTravelIn();
    }



    protected IEnumerator AlignVertical()
    {
        bool isAligned = false;
        bool fromUpSide = travelerObject.transform.position.y > gameObject.transform.position.y;
        float movementSpeed = fromUpSide ? -travelerAnimationSpeed : travelerAnimationSpeed;
        
        BoxCollider2D boxCollider = mainCollider as BoxCollider2D;
        BoxCollider2D travellerCollider = travelerCharacter.mainCollider as BoxCollider2D;

        float halfSize = boxCollider.size.y / 2;

        float halfTravelerSize = 
            travellerCollider.size.y / 2;

        centerMarker.transform.position = new Vector2(
            travelerObject.transform.position.x,
            gameObject.transform.position.y
        );

        while (!isAligned) {
            yield return new WaitForFixedUpdate();

            // AGRUPAR ESTE CÓDIGO EN UNA FUNCIÓN Y LLAMARLO
            // DESDE LAS DOS FUNCIONES DE ALINEAMIENTO

            float distance = Vector2.Distance(
                travelerObject.transform.position, 
                centerMarker.transform.position
            );

            if (distance <= travelerAnimationSpeed) {
                isAligned = true;
                movementSpeed = 0;
                travelerObject.transform.position = centerMarker.transform.position;
            }

            travelerObject.transform.Translate(new Vector2(0, movementSpeed));

            // HASTA AQUÍ....Y CONDICIONAR LA LÍNEA DE ARRIBA...
        }

        AnimateTravelIn();
    }



    protected IEnumerator AnimateTravel(DirectionsEnum direction, string goTo)
    {
        bool reachedDestination = false;
        bool _isHorizontal = goTo == "out" ? destinationPipe.isHorizontal : isHorizontal;

        Vector2 pipePosition = gameObject.transform.position;
        Vector2 destinationExitPosition = destinationPipe.centerMarker.transform.position;

        float movementSpeed = GetTravelSpeed(direction);

        Vector2 destination = goTo == "out" ? destinationExitPosition : pipePosition;

        while (!reachedDestination) {
            yield return new WaitForFixedUpdate();

            float distance = Vector2.Distance(
                travelerObject.transform.position,
                destination
            );

            Vector2 movementDirection = _isHorizontal
                ? new Vector3(movementSpeed, 0, travelerPositionZ) 
                : new Vector3(0, movementSpeed, travelerPositionZ);

            travelerObject.transform.Translate(movementDirection);

            if (isHorizontal && goTo == "in")
                reachedDestination = distance <= 1;
            else
                reachedDestination = distance <= travelerAnimationSpeed;
        }

        if (goTo == "out")
            ResetCharacterState();

        else
            SendToDestinationPipe();
    }



    protected void AnimateTravelOut()
    {
        destinationPipe.centerMarker.transform.localPosition = 
            centerMarker.transform.localPosition;

        DirectionsEnum destinationDirection = GetOppositeDirection(destinationPipe.exitDirection);
        
        if (destinationBGMAudioName != null) {
            StopBGM();
            PlayBGM(destinationBGMAudioName);
        }

        C_AnimateTravel = StartCoroutine(
            AnimateTravel(destinationDirection, "out")
        );
    }



    protected void AnimateTravelIn()
    {
        PlaySound("Pipe Travel");

        SetTravelerInPosition();

        C_AnimateTravel = StartCoroutine(
            AnimateTravel(exitDirection, "in")
        );
    }



    private float GetTravelSpeed(DirectionsEnum direction)
    {
        if (direction == DirectionsEnum.Up || direction == DirectionsEnum.Right)
            return -travelerAnimationSpeed;

        return travelerAnimationSpeed;
    }



    protected void SendToDestinationPipe()
    {
        travelerObject.transform.position = destinationPipeObject.transform.position;

        FixedPositionSetter positionSetter = Camera.main.GetComponent<FixedPositionSetter>();

        if (fixCameraOn != null)
            positionSetter.FixCameraOnPosition(fixCameraOn);
        else
            positionSetter.SetOnFixedPosition(false);

        AnimateTravelOut();
    }



    protected void SetChildrenPositions()
    {
        BoxCollider2D boxCollider = mainCollider as BoxCollider2D;
        exit.transform.localPosition = new Vector2(0, (boxCollider.size.y / 2) + 0.01f);
    }


    
    protected DirectionsEnum GetOppositeDirection(DirectionsEnum direction)
    {
        return direction == DirectionsEnum.Down ? DirectionsEnum.Up
            : direction == DirectionsEnum.Left ? DirectionsEnum.Right
            : direction == DirectionsEnum.Right ? DirectionsEnum.Left
            : DirectionsEnum.Down;
    }


    private bool PlayerIsOnTop()
    {
        float halfPipeSize = (mainCollider as BoxCollider2D).size.y / 2;
        float rayLength = halfPipeSize + 0.1f;

        RaycastHit2D raycast = Physics2D.Raycast(
            transform.position,
            Vector2.up,
            rayLength,
            LayerMask.GetMask("Player")
        );

        Debug.DrawRay(
            transform.position,
            Vector2.up * rayLength,
            Color.red
        );

        return raycast.collider != null;
    }


    // INSIDE ENEMY

    private void InstantiateEnemy()
    {
        if (!hasEnemy) return;

        enemyInstance = Instantiate(
            enemyPrefab, 
            transform.position, 
            transform.rotation,
            transform
        );
    }
    public void DisableEnemy()
    {
        if (enemyInstance == null || !hasEnemy) return;

        if (PlayerIsOnTop())
            enemyInstance.SetActive(false);
        else
            enemyInstance.SetActive(true);
    }


    private void SetTravelerInPosition()
    {
        previousTravelerPositionZ = travelerCharacter.sprite.transform.position.z;
        
        travelerCharacter.sprite.transform.position = new Vector3(
            travelerCharacter.sprite.transform.position.x,
            travelerCharacter.sprite.transform.position.y,
            travelerCharacter.sprite.transform.position.z + travelerPositionZ
        );
    }


    private void SetTravelerOutPosition()
    {
        travelerCharacter.sprite.transform.position = new Vector3(
            travelerCharacter.sprite.transform.position.x,
            travelerCharacter.sprite.transform.position.y,
            previousTravelerPositionZ
        );
    }

}
