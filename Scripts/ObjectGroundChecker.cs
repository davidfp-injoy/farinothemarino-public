﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGroundChecker : MonoBehaviour
{
    public float radius = 0.1f;
    public bool showRadius = false;
    public LayerMask grounds;

    private GameObject obj;
    private Rigidbody2D _rigidbody;
    private IOState iostate;
    private Jumping jump;
    private InteractiveObject interactiveObj;

    void FixedUpdate()
    {
        CheckGrounded();
    }

    void Awake()
    {
        obj         =  transform.parent.gameObject;
        iostate     =  obj.GetComponent<IOState>();
        jump        =  obj.GetComponent<Jumping>();
        _rigidbody  =  obj.GetComponent<Rigidbody2D>();
        interactiveObj =  obj.GetComponent<InteractiveObject>();
    }


    void OnDrawGizmos()
    {
        if (!showRadius) return;
        
        Gizmos.DrawWireSphere(transform.position, radius);
    }


    private void CheckGrounded()
    {
        iostate.isGrounded = Physics2D.OverlapCircle(
            transform.position,
            radius,
            grounds
        );

        interactiveObj.PlayAnim("isGrounded", iostate.isGrounded);
    }
}
