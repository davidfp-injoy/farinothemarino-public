﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerObject : InteractiveObject
{
    public string triggerAudioName;

    public override void Awake()
    {
        base.Awake();
    }

    protected void PlayTriggerAnim()
    {
        PlayAnim("isTriggered");
    }

    public abstract void Trigger();
}
