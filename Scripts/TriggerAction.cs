﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAction
{
    public GameObject obj;
    public bool done = false;

    public TriggerAction(GameObject _obj)
    {
        obj = _obj;
    }


    public void Trigger()
    {
        string type = obj.GetComponent<ObjectType>().type;

        if (type == null || type == "")
            throw new Exception("No se ha provisto el tipo de objeto. Debe proveerse el tipo de objeto correcto en el campo <type> del componente <ObjectType>");
        
        var behavior = obj.GetComponent(type) as TriggerObject;

        if (behavior == null)
            throw new Exception("No existe el componente con el nombre: " + type + " en el GameObject.");

        behavior.Trigger();

        done = true;
    }


}

