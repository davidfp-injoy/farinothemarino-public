﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollection : IOBehavior
{
    protected PlayerStats playerStats;

    protected override void AwakeAdditional()
    {
        base.AwakeAdditional();
        main = GetComponent<Character>();
    }


    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject obj = collider.gameObject;
        string tag = obj.tag;

        TriggeredActions(obj, tag);        
    }


    protected virtual void TriggeredActions(GameObject obj, string tag)
    {
        if (tag != "Item") return;

        TakeItem(obj);
        StopMovement(obj);
    }


    protected void TakeItem(GameObject itemObject)
    {
        TriggerAction action = new TriggerAction(itemObject);
        action.Trigger();
    }


    protected void StopMovement(GameObject item)
    {
        Rigidbody2D _rigidbody = item.GetComponent<Rigidbody2D>();
        
        if (_rigidbody == null) return;

        _rigidbody.simulated = false;
        _rigidbody.velocity = new Vector2(0, _rigidbody.velocity.y);
    }
}
