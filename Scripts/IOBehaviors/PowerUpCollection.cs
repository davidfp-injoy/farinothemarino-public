﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpCollection : ItemCollection
{
    protected override void TriggeredActions(GameObject obj, string tag)
    {
        if (tag != "PowerUp") return;

        StopMovement(obj);
        TakeItem(obj);

        (main as Player).SetPowerUp();
    }

}
