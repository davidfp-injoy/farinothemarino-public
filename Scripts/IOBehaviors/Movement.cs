using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : IOBehavior
{   
    public float movementSpeed = 2;
    public float runningSpeed = 2;
    public float onJumpSpeedMultiplier = 1;

    public bool canRun = true;
    public bool canRunOnAir = false;
    public bool autoTurnAroundOnCollision = false;
    public LayerMask autoTurnWhenHit;

    private bool isPlayer;
    

    protected override void AwakeAdditional()
    {
        isPlayer = main.GetComponent<Player>() != null;
    }


    void FixedUpdate()
    {
        Move();
    }



    void Update()
    {
        SetIsMoving();
        SetCurrentSpeed();
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        TurnAroundOnCollision();
    }



    private void Move()
    {
        if (main._rigidbody == null) return;

        if (main._rigidbody.bodyType == RigidbodyType2D.Static) return;

        main._rigidbody.velocity = new Vector2(iostate.currentSpeed, main._rigidbody.velocity.y);

        if (isPlayer
            && iostate.isGrounded
            && iostate.isMoving
            && main._rigidbody.simulated
            && !main.audioMan.controlAudio.isPlaying
        )
            main.PlaySound("Player Move", true, 0.5f);
            
    }



    public void SetCurrentSpeed()
    {
        string animState = "isMoving";

        if (!iostate.isMoving) {
            iostate.currentSpeed = 0;
            main.PlayAnim(animState, false);
            return;
        }

        float maxSpeed = 0;

        if (iostate.isRunning) {
            maxSpeed = iostate.currentDirection == "left" ? -runningSpeed : runningSpeed;
            animState = "isRunning";
        } else {
            maxSpeed = iostate.currentDirection == "left" ? -movementSpeed : movementSpeed;
        }

        main.PlayAnim(animState, true);

        iostate.currentSpeed = maxSpeed * (iostate.isGrounded ? 1 : onJumpSpeedMultiplier);
    }



    private void SetIsMoving()
    {
        iostate.isMoving = iostate.currentDirection == "right" 
            || iostate.currentDirection == "left" ;
    }



    public void GoTo(string direction = null)
    {
        if (direction != "left" && direction != "right")
            direction = null;

        main.SetLookingDir(direction);
    }



    public void Stop()
    {
        GoTo();
        main.StopSound();
    }



    public void SetRunning(bool _isRunning = false)
    {
        if (!canRunOnAir && !iostate.isGrounded)
            return;

        if (main._rigidbody.velocity.x == 0)
            _isRunning = false;

        bool val = canRun ? _isRunning : false;

        iostate.isRunning = val;
            
        main.PlayAnim("isRunning", val);
    }



    public void TurnAroundOnCollision()
    {        
        if (!autoTurnAroundOnCollision 
            || main._rigidbody.velocity.x > 0.1f
            || main._rigidbody.velocity.x < -0.1f
        )
            return;

        StartCoroutine("DelayedTurnAround");
    }


    public IEnumerator DelayedTurnAround()
    {
        yield return new WaitForSeconds(0.1f);

        if (!autoTurnAroundOnCollision 
            || main._rigidbody.velocity.x > 0.1f
            || main._rigidbody.velocity.x < -0.1f
        ) {
            yield return null;
        } else {
            bool goingToRight = iostate.currentDirection == "right";
            
            bool goingToLeft = iostate.currentDirection == "left";

            if (goingToRight)
                main.SetLookingDir("left");
            else if (goingToLeft)
                main.SetLookingDir("right");
        }
    }

}
