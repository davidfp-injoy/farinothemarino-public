﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Jumping : IOBehavior
{
    public float jumpSpeed = 0;
    public float jumpMaxTime = 1f;

    // CREO QUE ESTABA TOTALMENTE DORMIDO PORQUE NO EXISTE "MULTIPLAYER"
    // LO DEJARÉ ASÍ POR LOS LOLZ PERO DEBERÍA SER "multiplier", DE "multiplicador" LEL
    public float moveMultiplayer = 1.1f;
    public float runMultiplayer = 1.35f;

    [HideInInspector]
    public float jumpTimer;

    
    void Update()
    {}


    public void Jump(bool jumpStarted = true, bool jumpContinues = true, bool jumpEnded = false)
    {
        if (jumpStarted && iostate.isGrounded) {
            iostate.isJumping = true;
            main.PlayAnim("isJumping", true);
            main.PlaySound("Player Jump");
        }

        if (!iostate.isJumping)
            return;

        if (main.GetType() == typeof(Character))
            (main as Character).SetJumpingCollider();

        if (jumpEnded || jumpTimer <= 0)
            StopJumping();

        // Debug.Log(jumpTimer);

        if (jumpTimer > 0)
            jumpTimer -= Time.deltaTime;

        if (jumpContinues) {
            float jumpMultiplier = 1f;

            if (iostate.isMoving)
                jumpMultiplier = iostate.isRunning ? runMultiplayer : moveMultiplayer;

            iostate.currentJumpSpeed = jumpSpeed * jumpMultiplier;

            main._rigidbody.velocity = new Vector2(main._rigidbody.velocity.x, iostate.currentJumpSpeed);
        }
    }


    public void StopJumping()
    {
        iostate.isJumping = false;
        iostate.currentJumpSpeed = 0;
        // Debug.Log("=========================");
        // Debug.Log("JUMP ENDED");
    }


    public void RebootJumpTimer()
    {
        jumpTimer = jumpMaxTime;
    }

}
