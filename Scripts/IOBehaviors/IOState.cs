﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IOState : MonoBehaviour
{
    [HideInInspector]
    public string isLookingAt;

    // MOVEMENT
    [HideInInspector]
    public bool isMoving = false;

    [HideInInspector]
    public float currentSpeed = 0;

    [HideInInspector]
    public string currentDirection = null;

    [HideInInspector]
    public bool isRunning = false;


    // JUMP
    [HideInInspector]
    public bool isJumping = false;

    [HideInInspector]
    public float currentJumpSpeed = 0;

    [HideInInspector]
    public bool isGrounded = false;

    // TRAVEL
    [HideInInspector]
    public bool isTraveling = false;

    // DESTRUCTIBLE
    // [HideInInspector]
    public bool isImmune = false;
    [HideInInspector]
    public bool isUntouchable = false;

    // CHARACTER STATE
    [HideInInspector]
    public bool isGrown = false;


    public void ResetState()
    {
        currentDirection = null;
        isImmune = isMoving = isRunning = isJumping = isTraveling = isUntouchable = false;
        currentJumpSpeed = currentSpeed = 0;
    }

}
