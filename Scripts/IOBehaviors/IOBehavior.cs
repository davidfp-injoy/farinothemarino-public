﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IOBehavior : MonoBehaviour
{
    protected InteractiveObject main;
    protected IOState iostate;

    protected void Awake()
    {
        main = gameObject.GetComponent<InteractiveObject>();
        iostate = gameObject.GetComponent<IOState>();

        AwakeAdditional();
    }


    protected virtual void AwakeAdditional() {}
}
