﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Item : TriggerObject
{
    public int initPoints;

    public bool canMove;

    public string initialDirection;

    protected Movement movement;

    protected HUD hud;

    [HideInInspector]
    public int points;

    [HideInInspector]
    public GameObject spawnSound;

    protected GameObject playerObj;
    protected PlayerStats playerStats;
    
    private GameObject itemsTextPrefab;

    
    public override void Awake()
    {
        base.Awake();
        points      = initPoints;
        movement    = gameObject.GetComponent<Movement>();
        playerObj   = GameObject.FindGameObjectWithTag("Player");
        playerStats = playerObj.GetComponent<PlayerStats>();
        hud         = Camera.main.gameObject.GetComponent<HUD>();

        if (hud != null)
            itemsTextPrefab = hud.itemsTextObj;

        IgnoreCollisions();
    }


    public override void Start()
    {
        base.Start();
        gameObject.SetActive(true);
        Spawn();
        Move();
    }


    public void OnCollisionStay2D(Collision2D collision)
    {
        tag = collision.gameObject.tag;
        
        if (tag == "Block")
            SwitchDirectionOnBlockHit(collision.gameObject);
    }


    public void OnCollisionExit2D(Collision2D collision)
    {
        tag = collision.gameObject.tag;
        
        if (tag == "Block")
            SwitchDirectionOnBlockHit(collision.gameObject);
    }


    public override void Trigger()
    {
        SumPoints();
        PlayTriggerAnim();
        PlaySound(triggerAudioName);
        PlayShowTextAnim();
        TriggerAction();
        RunDispatch();
    }


    abstract protected void TriggerAction();


    protected void Move()
    {
        if (!canMove || movement == null || iostate == null) return;

        iostate.currentDirection = initialDirection;

        movement.GoTo(iostate.currentDirection);
    }


    public void Spawn()
    {
        // Esto probablemente ni haga falta, se coloca la animación como la "inicial" o como "raíz" o no sé, vos véis
        // animator.SetBool("isSpawned", true);
        // Debug.Log("Spawned: " + type);
    }

    public void SumPoints()
    {
        playerStats.score += points;
        string totalScore = playerStats.score.ToString();
        if (hud == null) return;
        hud.UpdateScore(totalScore);
    }

    protected void IgnoreCollisions()
    {
        int itemsLayer = LayerMask.NameToLayer("Item");
        Physics2D.IgnoreLayerCollision(itemsLayer, itemsLayer);
    }


    private void PlayShowTextAnim()
    {
        if (itemsTextPrefab == null) return;
        ShowScoreText();
    }


    protected void ShowScoreText()
    {        
        GameObject textInstance = Instantiate(itemsTextPrefab);

        Vector2 playerPos = playerObj.transform.position;

        textInstance.transform.position = new Vector2(
            playerPos.x - 1,
            playerPos.y + 1
        );

        TextMesh scoreText = itemsTextPrefab.GetComponentInChildren<TextMesh>();

        scoreText.text = points.ToString();
    }


    protected void SwitchDirectionOnBlockHit(GameObject blockObj)
    {
        if (movement == null || !canMove) return;

        Block block = blockObj.GetComponent<Block>();

        if (!block.isBeingHit) return;

        _rigidbody.velocity = new Vector2(0, 10);

        if (block.transform.position.x < transform.position.x)
            SetLookingDir("right");
        else
            SetLookingDir("left");
    }
}
