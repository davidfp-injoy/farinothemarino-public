using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloverGreen : Item
{
    override protected void TriggerAction()
    {
        playerStats.lives++;
        Debug.Log("Lives: " + playerStats.lives);
    }
}
