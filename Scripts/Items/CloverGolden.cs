using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloverGolden : PowerUpItem
{
    public override void Start()
    {
        base.Start();
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 10);
        movement.GoTo(initialDirection);
    }

    
    override protected void TriggerAction()
    {
        base.TriggerAction();
        StartCoroutine("ResetColor");
    }


    private IEnumerator ResetColor()
    {
        Player player = playerObj.GetComponent<Player>();
        yield return new WaitForSeconds(powerUpData.duration * 0.9f);
        player.playerSprites.ResetColor();
        player.colorAnimator.Clear();
    }
}
