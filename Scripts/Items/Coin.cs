﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Coin : Item
{
    override protected void TriggerAction()
    {
        playerStats.coins = playerStats.coins + 1;
        string totalCoins = playerStats.coins.ToString();
        if (hud == null) return;
        hud.UpdateCoins(totalCoins);
    }
}
