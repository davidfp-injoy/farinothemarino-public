﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DestructibleObject : InteractiveObject
{
    public int health = 1;

    public string dispatchAudioName;

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;

        if (tag == "Projectile") {
            Projectile projectile = collision.gameObject.GetComponent<Projectile>();
            GetDamageFromProjectile(projectile);
        }
    }


    public virtual void GetDamage(int damage)
    {
        health -= damage;

        if (dispatchAudioName != null)
            PlaySound(dispatchAudioName);

        if (health <= 0)
            RunDispatch();
    }

    public void GetDamageFromProjectile(Projectile projectile)
    {
        if (projectile == null) return;

        bool willGetDmg = Array.Exists<string>(
            projectile.canDamageTags,
            elem => elem == gameObject.tag
        );

        if (!willGetDmg) return;

        AdditionalGetDmgFromProjActions();

        projectile.RunDispatch();

        GetDamage(projectile.damage);
    }

    protected virtual void AdditionalGetDmgFromProjActions() {}

    public bool IsImmune()
    {
        return iostate.isImmune;
    }

    public void SetImmune(bool _isImmune)
    {
        iostate.isImmune = _isImmune;
    }
}
