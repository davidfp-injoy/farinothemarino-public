﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Projectile
{
    Jumping jump;

    public override void Awake()
    {
        shooterObject = GameObject.Find("Player");
        base.Awake();
    }

    public override void Start()
    {
        base.Start();
        Physics2D.IgnoreCollision(mainCollider, shooterObject.GetComponent<Collider2D>());
        jump = GetComponent<Jumping>();
        iostate.currentDirection = shooterIostate.isLookingAt;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;

        if (tag == "Ground" 
            || tag == "Platform" 
            || tag == "Block" 
            || tag == "Pipe")
            jump.Jump();
    }
}
