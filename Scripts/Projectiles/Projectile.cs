﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : InteractiveObject
{
    public int damage = 1;
    public string[] canDamageTags;

    [HideInInspector]
    public GameObject shooterObject;
    [HideInInspector]
    public IOState shooterIostate;
    [HideInInspector]
    public Movement movement;


    public override void Awake()
    {
        base.Awake();
        movement = GetComponent<Movement>();
        shooterIostate = shooterObject.GetComponent<IOState>();
    }
}
