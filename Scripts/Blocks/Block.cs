﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Block : TriggerObject
{
    public string dispatchAudioName;
    public int maxHits;
    public int currentHitsCount = 0;
    public bool isVisible = true;
    
    [HideInInspector]
    public bool isBeingHit = false;

    // Coroutines
    private Coroutine C_AnimateTriggered;
    private Coroutine C_SetIsBeingHit;
    

    public override void Awake()
    {
        base.Awake();
        sprite.SetActive(isVisible);
    }
        
    public override void Trigger()
    {
        if (isDispatched) return;
        C_SetIsBeingHit = StartCoroutine("SetIsBeingHit");

        sprite.SetActive(true);
        
        gameObject.SetActive(true);
        HitAction();

        C_AnimateTriggered = StartCoroutine("AnimateTriggered");

        currentHitsCount++;
        
        if (currentHitsCount >= maxHits) {
            RunDispatch();
    
            if (dispatchAudioName != null)
                PlaySound(dispatchAudioName);
        } else {
    
            if (dispatchAudioName != null)
                PlaySound(dispatchAudioName);
        }
    }
    

    protected virtual void HitAction() { }


    protected IEnumerator AnimateTriggered()
    {
        if (animator) {
            animator.SetBool("isTriggered", false);
            animator.SetBool("isTriggered", true);
            yield return new WaitForSeconds(0.2f);
            animator.SetBool("isTriggered", false);
        } else {
            yield return new WaitForEndOfFrame();
        }
    }


    protected IEnumerator SetIsBeingHit()
    {
        isBeingHit = true;
        yield return new WaitForSeconds(0.1f);
        isBeingHit = false;
    }

}
