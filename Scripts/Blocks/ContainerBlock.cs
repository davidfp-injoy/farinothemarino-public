﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerBlock : Block
{
    public GameObject containedItem;
    public bool autoTriggerItem = false;
    private GameObject instantiatedItem;

    public override void Awake()
    {
        base.Awake();
    }

    protected override void HitAction()
    {
        if (containedItem == null) return;

        Vector2 itemPos = gameObject.transform.position;
        Vector2 pos = new Vector2(itemPos.x, itemPos.y + 2);

        instantiatedItem = Instantiate(containedItem, pos, containedItem.transform.rotation);

        if (autoTriggerItem)
            StartCoroutine("TriggerContainedItem");
    }


    private IEnumerator TriggerContainedItem()
    {
        yield return new WaitForSeconds(0.2f);

        TriggerAction action = new TriggerAction(instantiatedItem);
        action.Trigger(); 
    }
}