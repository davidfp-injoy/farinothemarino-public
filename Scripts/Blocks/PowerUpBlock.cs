﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBlock : ContainerBlock
{
    public GameObject alternativeItem;
    protected bool hasMainPowerUp = false;
    protected PowerUp playerPowerUp;


    protected override void HitAction()
    {
        if (playerPowerUp == null || alternativeItem == null) {
            base.HitAction();
            return;
        }

        PowerUpData mainPowerUp = containedItem.GetComponent<PowerUpData>();

        if (playerPowerUp.sprite == mainPowerUp.sprite)
            containedItem = alternativeItem;

        base.HitAction();
    }


    public void SetPlayerPowerUp(PowerUp powerUp)
    {
        playerPowerUp = powerUp;
    }
}
