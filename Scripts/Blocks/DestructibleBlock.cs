﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleBlock : Block
{
    protected override IEnumerator Dispatch()
    {
        DisableCollider();
        yield return new WaitForSeconds(2f);
        base.Dispatch();
    }

}
