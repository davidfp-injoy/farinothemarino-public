﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : InteractiveObject
{
    public int world = 1;
    public int level = 1;
    private GameObject flagObject;
    private GameObject stickObject;
    private GameObject playerObject;
    private GameObject bottomObject;
    private GameObject topObject;
    private bool isPuttingFlagDown = false;
    private float fallSpeed = 0.2f;
    private float stickHeight = 0;
    private float playerScore = 0;
    private float showingPlayerScore = 0;
    private GameObject textPrefab;


    public override void Awake()
    {
        base.Awake();
        flagObject   = transform.Find("Flag").gameObject;
        stickObject  = transform.Find("Stick").gameObject;
        bottomObject = transform.Find("Bottom").gameObject;
        topObject    = transform.Find("Top").gameObject;
        stickHeight  = CalculateHeight();
        SetTextPrefab();
    }

    void SetTextPrefab()
    {
        HUD hud = Camera.main.gameObject.GetComponent<HUD>();

        if (hud != null)
            textPrefab = hud.itemsTextObj;
    }

    void FixedUpdate()
    {
        Fall();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        string tag = collider.gameObject.tag;
            
        if (tag == "Player") {
            AudioManager bgmMan = Camera.main.GetComponent<AudioManager>();
            bgmMan.Stop();
            bgmMan.Play("Success OST");

            playerObject = collider.gameObject;

            Player player = playerObject.GetComponent<Player>();
            player._rigidbody.simulated = false;
            player.StopControls();

            playerObject.transform.position = new Vector2(
                stickObject.transform.position.x,
                playerObject.transform.position.y
            );

            playerScore = GetPlayerScore();
            showingPlayerScore = playerScore;

            BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();
            boxCollider.enabled = false;

            StartCoroutine("SetPutFlagDown");
        }
    }


    IEnumerator SetPutFlagDown()
    {
        yield return new WaitForSeconds(1);
        isPuttingFlagDown = true;
        StartCoroutine("SetShowingPlayerScore");
    }


    public void StopFlagGoingDown()
    {
        isPuttingFlagDown = false;
    }

    // * PERSISTIR ANIMACIONES DE Golden Clover EN SPRITE GRANDE

    // * AÑADIR PANTALLA DE CRÉDITOS CON EXPLICACIÓN DEL EJERCICIO 


    public void Fall()
    {
        if (!isPuttingFlagDown) return;
        playerObject.transform.Translate(Vector2.down * fallSpeed);
        
        flagObject.transform.position = new Vector2(
            flagObject.transform.position.x,
            flagObject.transform.position.y - fallSpeed
        );

        float distance = GetPlayerHeight();

        if (distance <= (fallSpeed * 2))
            StartCoroutine(ReachGoalBottom());
    }


    public IEnumerator ReachGoalBottom()
    {
        StopFlagGoingDown();

        yield return new WaitForSeconds(1);

        playerObject.transform.position = new Vector2(
            playerObject.transform.position.x + 1,
            playerObject.transform.position.y
        );
        
        Player player = playerObject.GetComponent<Player>(); 
        player._rigidbody.simulated = true;
        
        Movement playerMovement = playerObject.GetComponent<Movement>();
        playerMovement.GoTo("right");
    }

    private float GetPlayerScore()
    {
        float playerHeight = GetPlayerHeight();
        return CalculateHeight(playerHeight);
    }

    private float GetPlayerHeight()
    {
        return Vector2.Distance(
            bottomObject.transform.position,
            playerObject.transform.position
        );
    }

    private float CalculateHeight(float baseHeight = 0)
    {
        float fullHeight = Vector2.Distance(
            topObject.transform.position,
            bottomObject.transform.position
        );

        float height = baseHeight == 0 ? fullHeight : baseHeight;

        float heightPercent = (height * 100) / fullHeight;

        float baseCalculatedHeight = 8000 * (heightPercent / 100);

        float calculatedHeight = Mathf.RoundToInt(baseCalculatedHeight);
        
        return calculatedHeight;
    }

    private IEnumerator SetShowingPlayerScore()
    {
        float tick = 0.2f;

        while (isPuttingFlagDown) {
            yield return new WaitForSeconds(tick);

            float playerHeight = GetPlayerHeight();
            
            showingPlayerScore = CalculateHeight(playerHeight);

            ShowScoreText(showingPlayerScore);
        }
    }


    protected void ShowScoreText(float points)
    {        
        GameObject textInstance = Instantiate(textPrefab);

        Vector2 playerPos = playerObject.transform.position;

        textInstance.transform.position = new Vector2(
            playerPos.x - 1,
            playerPos.y + 1
        );

        TextMesh scoreText = textPrefab.GetComponentInChildren<TextMesh>();

        scoreText.text = points.ToString();
    }
}
