using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDamageDealer : DamageDealer
{
    void Update()
    {
        _collider.enabled = player._rigidbody.velocity.y < 0;
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        SmashEnemy(collider);
    }

    private void SmashEnemy(Collider2D collider)
    {
        if (collider.gameObject.tag != "Enemy") return;

        Enemy enemy = collider.gameObject.GetComponent<Enemy>();

        if (enemy.canBeSmashed) {
            enemy.GetSmashed();
            // StartCoroutine("SetPlayerImmunity");
        } else {
            player.GetDamage(enemy.damageOnContact);
        }
    }

    private IEnumerator SetPlayerImmunity()
    {
        player.SetImmune(true);
        yield return new WaitForSeconds(0.2f);
        player.SetImmune(false);
    }
}