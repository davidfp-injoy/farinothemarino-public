﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDamageDealer : DamageDealer
{
    void Update()
    {
        _collider.enabled = player._rigidbody.velocity.y > 0;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        HitBlock(collision);
    }

    private void HitBlock(Collision2D collision)
    {
        if (collision.gameObject.tag != "Block") return;

        TriggerAction action = new TriggerAction(collision.gameObject);

        Jumping parentJumping = transform.parent.GetComponent<Jumping>();

        PowerUpBlock powerUpBlock = collision.gameObject.GetComponent<PowerUpBlock>();

        if (powerUpBlock != null)
            powerUpBlock.SetPlayerPowerUp(player.playerStats.currentPowerUp);

        if (parentJumping != null)
            parentJumping.StopJumping();

        action.Trigger();
    }
}
