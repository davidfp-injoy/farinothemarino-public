﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteColorAnimator : MonoBehaviour
{
    private bool playing = false;

    private List<SpriteRenderer[]> recoloredRenderersList = new List<SpriteRenderer[]>();

    public IEnumerator Lerp(GameObject sprite, Color[] colors, float speed)
    {
        yield return new WaitForSeconds(0.1f);
        playing = true;

        SpriteRenderer[] renderers = GetRenderers(sprite);

        if (renderers.Length == 0) 
            Stop();

        SpriteRenderer lastRenderer = renderers.Length > 0 
            ? renderers[renderers.Length - 1] 
            : null;

        float frameInterval = speed / 60;

        while (playing) {
            int i = 0;
            while (i < colors.Length && playing) {
                bool isLastIndex = i == colors.Length - 1;
                int nextIndex = isLastIndex ? 0 : i + 1;
                float finalSpeed = frameInterval;
                Color step = Color.white;

                while (step != colors[nextIndex] && playing) {
                    yield return new WaitForFixedUpdate();

                    finalSpeed += frameInterval;

                    step = Color.Lerp(colors[i], colors[nextIndex], finalSpeed);
                    
                    Recolor(step, renderers);
                }

                if (isLastIndex) i = 0;
                else i++;
            }
        }
        
        yield return new WaitForSeconds(speed * 2);
        Recolor(Color.white, renderers);
        Clear();
    }


    public IEnumerator Switch(GameObject sprite, Color[] colors, float interval)
    {
        yield return new WaitForSeconds(0.1f);
        playing = true;

        SpriteRenderer[] renderers = GetRenderers(sprite);

        if (renderers.Length == 0) 
            Stop();

        while (playing) {
            int i = 0;
            while (i < colors.Length) {
                if (!playing) {
                    Stop();
                    break;
                }

                yield return new WaitForSeconds(interval);
                bool isLastIndex = i == colors.Length - 1;
                
                Recolor(colors[i], renderers);

                if (isLastIndex) i = 0;
                else i++;
            }
        }

        yield return new WaitForSeconds(interval);
        Recolor(Color.white, renderers);
        Clear();
    }


    public void Play(GameObject sprite, Color[] colors, float speed)
    {
        StartCoroutine(Lerp(sprite, colors, speed));
    }


     public void PlaySwitch(GameObject sprite, Color[] colors, float speed)
    {
        StartCoroutine(Switch(sprite, colors, speed));
    }


    public void Stop()
    {
        playing = false;
        Clear();
    }


    public void Recolor(Color color, SpriteRenderer[] renderers)
    {
        foreach(SpriteRenderer renderer in renderers) {
            renderer.color = color;
            renderer.material.color = color;
        }
    }


    public void Clear()
    {
        foreach(SpriteRenderer[] renderers in recoloredRenderersList) {
            Recolor(Color.white, renderers);
        }

        recoloredRenderersList.Clear();
    }


    public SpriteRenderer[] GetRenderers(GameObject sprite)  
    {
        SpriteRenderer[] renderers = sprite.transform.GetComponentsInChildren<SpriteRenderer>();
        recoloredRenderersList.Add(renderers);
        return renderers;
    }


    public bool isPlaying()
    {
        return playing;
    }

}
