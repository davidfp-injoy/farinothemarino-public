﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharacterController
{
    Player player;

    public bool canUsePowerUpAction = true;
    Coroutine C_ResetCanUsePowerUpAction;


    protected override void Awake()
    {
        base.Awake();
        player = gameObject.GetComponent<Player>();
    }


    protected override void Update()
    {
        base.Update();
        UsePowerUpAction();
    }


    void OnTriggerStay2D(Collider2D collider)
    {
        string tag = collider.gameObject.tag;

        if (tag == "PipeExit")
            GetInPipe(collider);
    }



    protected override void Move()
    {
        if (movement == null) return;

        // float inputDir = Input.GetAxis("Horizontal");
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A)) {
            movement.Stop();
        }

        // if (inputDir > 0 && movement.currentDirection != "right") {
        if (Input.GetKey(KeyCode.D)) {
            movement.GoTo("right");
        }
        
        // if (inputDir < 0 && movement.currentDirection != "left") {
        if (Input.GetKey(KeyCode.A)) {
            movement.GoTo("left");
        }
    }


    protected override void Run()
    {
        if (movement == null) return;
        
        bool isPressingRunButton = Input.GetKey(KeyCode.LeftShift) || Input.GetButton("Fire1");
        movement.SetRunning(isPressingRunButton);
    }


    protected override void Jump()
    {
        if (jump == null) return;

        bool jumpStarted = Input.GetButtonDown("Jump");
        bool jumpContinues = Input.GetButton("Jump");
        bool jumpEnded = Input.GetButtonUp("Jump");

        if (!jumpStarted && !jumpContinues && !jumpEnded)
            return;

        jump.Jump(
            jumpStarted,
            jumpContinues,
            jumpEnded
        );
    }



    protected void GetInPipe(Collider2D collider)
    {
        if (iostate.isTraveling) return;

        if (   Input.GetKeyDown(KeyCode.W) || Input.GetKey(KeyCode.W)
            || Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A)
            || Input.GetKeyDown(KeyCode.S) || Input.GetKey(KeyCode.S)
            || Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D)
        ) {
            string keyPressed = 
              Input.GetKeyDown(KeyCode.W) || Input.GetKey(KeyCode.W) ? "W"
            : Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A) ? "A"
            : Input.GetKeyDown(KeyCode.S) || Input.GetKey(KeyCode.S) ? "S"
            : "D";
            
            GameObject pipeObject = collider.transform.parent.gameObject;

            bool pressedCorrectButton = 
                PressedCorrectTravelButton(keyPressed, pipeObject);
            
            player.TravelOnPipe(pipeObject, pressedCorrectButton);
        }
    }


    protected bool PressedCorrectTravelButton(string keyPressed, GameObject pipeObject)
    {
        Pipe pipe = pipeObject.GetComponent<Pipe>();

        return keyPressed == "W" && pipe.exitDirection == DirectionsEnum.Down ? true
             : keyPressed == "A" && pipe.exitDirection == DirectionsEnum.Right ? true
             : keyPressed == "S" && pipe.exitDirection == DirectionsEnum.Up ? true
             : keyPressed == "D" && pipe.exitDirection == DirectionsEnum.Left ? true
             : false;
    }


    protected void UsePowerUpAction()
    {
        if (!Input.GetButtonDown("Fire1") || !canUsePowerUpAction) return;

        PowerUp currentPowerUp = player.playerStats.currentPowerUp;
        
        currentPowerUp.Action();

        if (currentPowerUp.actionCooldown <= 0 || C_ResetCanUsePowerUpAction != null) return;

        canUsePowerUpAction = false;
        
        C_ResetCanUsePowerUpAction = StartCoroutine(ResetCanUsePowerUpAction());
    }

    private IEnumerator ResetCanUsePowerUpAction()
    {
        float actionCooldown = player.playerStats.currentPowerUp.actionCooldown;

        yield return new WaitForSeconds(actionCooldown);

        canUsePowerUpAction = true;

        C_ResetCanUsePowerUpAction = null;
    }


}
