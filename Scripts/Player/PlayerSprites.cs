﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprites
{
    public GameObject small;
    public GameObject grown;
    public GameObject fire;
    public GameObject dead;

    [HideInInspector]
    public GameObject current;

    [HideInInspector]
    public GameObject previous;

    [HideInInspector]
    public Animator animator;

    [HideInInspector]
    public Animation animation;

    public SpriteColorAnimator colorAnimator;


    public PlayerSprites(SpritesStruc spritesList, SpriteColorAnimator colorAnimator)
    {
        small = spritesList.small;
        grown = spritesList.grown;
        fire = spritesList.fire;
        dead = spritesList.dead;

        this.colorAnimator = colorAnimator;

        SetSmall();
    }

    private void UpdateSprite(GameObject spriteObject)
    {
        previous = current;
        current = spriteObject;
        animator = current.GetComponent<Animator>();
        animation = current.GetComponent<Animation>();
        
        if (previous != null)
            previous.SetActive(false);
        
        current.SetActive(true);
    }

    public void SetByName(SpriteName spriteName)
    {
        small.SetActive(false);
        grown.SetActive(false);
        fire.SetActive(false);
        
        switch (spriteName) {
            case SpriteName.Small:
                SetSmall();
                break;
            case SpriteName.Grown:
                SetGrown();
                break;
            case SpriteName.Fire:
                SetFire();
                break;
            case SpriteName.Dead:
                SetDead();
                break;
            case SpriteName.Invincible:
                SetInvincible();
                break;
            case SpriteName.TemporalImmunity:
                SetTemporalImmunity();
                break;
        }
    }

    public void SetSmall()
    {
        UpdateSprite(small);
    }

    public void SetGrown()
    {
        UpdateSprite(grown);
    }

    public void SetFire()
    {
        UpdateSprite(fire);
    }

    public void SetDead()
    {
        UpdateSprite(dead);
    }

    public void SetInvincible()
    {
        float multiplier = 1;

        Color red     = Color.red * multiplier;
        Color magenta = Color.magenta * multiplier;
        Color blue    = Color.blue * multiplier;
        Color green   = Color.green * multiplier;
        Color yellow  = Color.yellow * multiplier;

        red.a = magenta.a = blue.a = green.a = yellow.a = 1;

        Color[] colors = { red, magenta, blue, green, yellow };
        
        colorAnimator.PlaySwitch(current, colors, 0.2f);
    }

    public void SetTemporalImmunity()
    {
        Color[] colors = { Color.white, Color.white * 0.5f };
        colorAnimator.PlaySwitch(small, colors, 0.07f);
    }


    public void ResetColor()
    {
        colorAnimator.Stop();
        SpriteRenderer[] renderers = colorAnimator.GetRenderers(current);
        Color color = Color.white;
        color.a = 1;
        colorAnimator.Recolor(color, renderers);
    }


    public IEnumerator DelayedResetColor(float delay)
    {
        Debug.Log(Time.time);
        yield return new WaitForSeconds(delay);
        Debug.Log(Time.time);
        ResetColor();
    }
}
