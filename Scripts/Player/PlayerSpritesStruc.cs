using UnityEngine;

[System.Serializable]

public struct SpritesStruc
{
    public GameObject small;
    public GameObject grown;
    public GameObject fire;
    public GameObject dead;
}