using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    public string _name;
    
    [HideInInspector]
    public Player player;

    public Collider2D _collider;

    void Awake()
    {
        player = transform.parent.GetComponent<Player>();

        _collider = GetComponent<Collider2D>();

        Physics2D.IgnoreCollision(
            GetComponent<BoxCollider2D>(),
            transform.parent.gameObject.GetComponent<BoxCollider2D>()
        );
    }
}