using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : Character
{
    public float InDamageImmunityTime = 3;

    // PLAYER SPRITES
    [Header("Sprites")]
    public SpritesStruc spritesList;

    public PlayerSprites playerSprites;

    [HideInInspector]
    public SpriteColorAnimator colorAnimator;
    
    [HideInInspector]
    public PlayerStats playerStats;

    public float sizeChangeTime = 1;

    public GameObject fireballObj;


    // COROUTINES
    Coroutine C_PowerUpTimer;


    public override void Awake()
    {
        LevelTimer levelTimer = Camera.main.GetComponent<LevelTimer>();
        if (levelTimer) lifespan = levelTimer.time;

        base.Awake();

        playerStats = gameObject.GetComponent<PlayerStats>();
        
        colorAnimator = gameObject.GetComponent<SpriteColorAnimator>();

        playerSprites = new PlayerSprites(spritesList, colorAnimator);
        playerSprites.SetByName(SpriteName.Small);
        
        spriteInitScale = playerSprites.current.transform.localScale.x;
        playerStats.currentPowerUp = playerStats.savedPowerUp = new PowerUp();
        SetDamageDealersPosition();

        if (playerSprites.current.transform.localScale.x == -1)
            iostate.isLookingAt = "left";
            
        else if (playerSprites.current.transform.localScale.x == 1)
            iostate.isLookingAt = "right";
    
        IgnoreCollisions();
    }


    void FixedUpdate()
    {
        EnableFireball();
    }


    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        string tag = collision.gameObject.tag;
        
        CollisionEnemyActions(collision, tag);

        if (tag == "WorldBottom") {
            Die();
            GameObject spritesArrObj = transform.GetChild(0).gameObject;
            spritesArrObj.SetActive(false);
        }
    }


    protected void OnCollisionExit2D(Collision2D collision)
    {
        CollisionEnemyActions(collision, tag);
    }


    private void CollisionEnemyActions(Collision2D collision, string tag)
    {
        if (tag != "Enemy") return;

        Enemy enemy = collision.gameObject.GetComponent<Enemy>();

        string enemyType = enemy.GetComponent<ObjectType>().type;
        
        if (iostate.isUntouchable)
            enemy.GetAbsoluteDamage(3);
        else if (enemyType == "BadTurtle" && !enemy.iostate.isMoving)
            (enemy as BadTurtle).MoveInShell();
        else
            GetDamage(enemy.damageOnContact);
    }


    private void SwitchFixedCameraPosition(string action, GameObject fixedPositionObj) {
        if (action != "in" && action != "out") {
            Debug.LogWarning("Parámetro 'action' no es válido");
            return;
        }
        
        FixedPositionSetter positionSetter = Camera.main.GetComponent<FixedPositionSetter>();
        if (positionSetter == null) return;

        if (action == "in")
            positionSetter.FixCameraOnPosition(fixedPositionObj);
        else if (action == "out")
            positionSetter.SetOnFixedPosition(false);
    }


    // POWER UPS
    public void SetPowerUp(bool recursive = false)
    {
        if (playerStats.currentPowerUp.getsGrown) {
            if (!recursive) {
                StartCoroutine("SetPowerUpWithGrowing");
                return;
            }
        }

        if (playerStats.currentPowerUp.sprite != SpriteName.Initial) {
            playerSprites.SetByName(playerStats.currentPowerUp.sprite);
            sprite = playerSprites.current;
        }

        iostate.isGrown = playerStats.currentPowerUp.getsGrown;
        iostate.isUntouchable = playerStats.currentPowerUp.getsUntouchable;

        SetImmune(playerStats.currentPowerUp.getsImmune);

        if (playerStats.currentPowerUp.duration > 0)
            C_PowerUpTimer = StartCoroutine("PowerUpTimer");

        if (iostate.isGrounded)
            SetGroundedCollider();
        else
            SetJumpingCollider();

        SetDamageDealersPosition();
    }


    public IEnumerator SetPowerUpWithGrowing()
    {
        if (!playerStats.currentPowerUp.getsGrown) yield return null;

        StartCoroutine("PlayGrowingAnim");

        yield return new WaitForSeconds(1);

        SetPowerUp(true);
    }


    public IEnumerator LoosePowerUpWithDwarfing()
    {
        if (!playerStats.currentPowerUp.getsGrown) yield return null;

        StartCoroutine("PlayDwarfingAnim");
        
        yield return new WaitForSeconds(1);

        LoosePowerUp(true);
    }


    private IEnumerator PowerUpTimer()
    {
        yield return new WaitForSeconds(playerStats.currentPowerUp.duration);

        LoosePowerUp();
        Debug.Log("PU Timeout!");

        StopCoroutine("C_PowerUpTimer");
        C_PowerUpTimer = null;
    }


    private void LoosePowerUp(bool recursive = false)
    {
        if (playerStats.currentPowerUp.getsGrown) {
            if (!recursive) {
                StartCoroutine("LoosePowerUpWithDwarfing");
                return;
            }
        }

        if (bgmMan.currentAudioName != "Main OST") {
            bgmMan.Stop();
            bgmMan.Play("Main OST");
        }
        
        if (iostate.isUntouchable) {
            SetDefaultStatsFromUntouchable();
        } else {
            StartCoroutine("SetTemporaryImmunity");

            if (playerStats.currentPowerUp.sprite != SpriteName.Initial)
                SetDefaultStats();
        }

        Debug.Log(playerStats.savedPowerUp.getsGrown);
    }

    
    private IEnumerator SetTemporaryImmunity()
    {
        int playerLayer = LayerMask.NameToLayer("Player");
        int enemiesLayer = LayerMask.NameToLayer("Enemy");
        int fallDmgDealerLayer = LayerMask.NameToLayer("Fall Damage Dealer");

        SetImmune(true);
        playerSprites.colorAnimator.Stop();

        yield return new WaitForFixedUpdate();
        playerSprites.SetTemporalImmunity();
        Physics2D.IgnoreLayerCollision(playerLayer, enemiesLayer);
        Physics2D.IgnoreLayerCollision(fallDmgDealerLayer, enemiesLayer);

        yield return new WaitForSeconds(InDamageImmunityTime);
        SetImmune(false);
        Physics2D.IgnoreLayerCollision(playerLayer, enemiesLayer, false);
        Physics2D.IgnoreLayerCollision(fallDmgDealerLayer, enemiesLayer, false);
        
        SetDefaultStats();
    }

    private void ResetColor(float delay = 0)
    {
        playerSprites.colorAnimator.Stop();
        StartCoroutine(playerSprites.DelayedResetColor(delay));
    }


    public void SetDefaultStats()
    {
        playerSprites.SetByName(SpriteName.Small);
        sprite = playerSprites.current;

        health = 1;
        
        iostate.isGrown = false;

        ResetColor(0.1f);

        SetColliderSize();
        SetDamageDealersPosition();

        playerStats.currentPowerUp 
            = playerStats.savedPowerUp
            = new PowerUp();
    }


    public void SetDefaultStatsFromUntouchable()
    {
        iostate.isUntouchable = false;
        SetImmune(false);
        ResetColor();
        playerStats.currentPowerUp = playerStats.savedPowerUp;
    }


    public override void PlayAnim(string param, bool value = true)
    {
        if (playerSprites.animator == null) return;
        playerSprites.animator.SetBool(param, value);
    }


    // DAMAGE DEALERS
    public void SetDamageDealersPosition()
    {
        GameObject damageDealerObj = damageDealers[0];

        if (damageDealerObj == null) return;

        float ySize = iostate.isGrown ? 2.36f : 0.77f;

        damageDealerObj.transform.localPosition = new Vector2(0, ySize);
    }

    
    private void IgnoreCollisions()
    {
        Physics2D.IgnoreCollision(mainCollider, mainTrigger);

        for (int i = 0; i < damageDealers.Length; i++) {
            GameObject _obj = damageDealers[i];
            DamageDealer dd = _obj.GetComponent<DamageDealer>();
        
            Physics2D.IgnoreCollision(
                mainCollider,
                transform.Find(dd._name).GetComponent<BoxCollider2D>()
            );

            Physics2D.IgnoreCollision(
                mainTrigger,
                transform.Find(dd._name).GetComponent<BoxCollider2D>()
            );
        }
    }


    public override void GetDamage(int damage = 1)
    {
        if (iostate.isImmune) return;

        if (playerStats.currentPowerUp.isSet) {
            LoosePowerUp();
            return;
        }

        Die();
    }


    protected override IEnumerator RunLifeTime()
    {
        yield return new WaitForSeconds(lifespan);
        dispatchTime = 0;
        Die();
    }


    void Die()
    {
        StopControls();
        StopBGM();
        playerSprites.SetByName(SpriteName.Dead);
        _rigidbody.simulated = false;
        base.GetDamage(health);
        StartCoroutine("WaitForGameOver");
    }


    private IEnumerator WaitForGameOver()
    {
        yield return new WaitForSeconds(2);
        Debug.Log("LOADING SCENE...");
        SceneManager.LoadScene("Title");
    }


    private IEnumerator PlayGrowingAnim()
    {
        List<GameObject> list = GetSurroundingMovingObjects();
        
        playerSprites.animator.SetBool("isGrowing", true);
        _rigidbody.simulated = false;

        PauseSurroundingObjects(list);
        
        yield return new WaitForSeconds(sizeChangeTime);

        _rigidbody.simulated = true;
        playerSprites.animator.SetBool("isGrowing", false);

        ResumeSurroundingObjects(list);
        playerSprites.ResetColor();
    }


    private IEnumerator PlayDwarfingAnim()
    {
        List<GameObject> list = GetSurroundingMovingObjects();
        
        playerSprites.animator.SetBool("isDwarfing", true);
        _rigidbody.simulated = false;

        PauseSurroundingObjects(list);
        
        yield return new WaitForSeconds(1f);

        _rigidbody.simulated = true;
        playerSprites.animator.SetBool("isDwarfing", false);

        ResumeSurroundingObjects(list);
        ResetColor(0.1f);
        // playerSprites.ResetColor();
    }


    private void PauseSurroundingObjects(List<GameObject> list)
    {
        foreach (GameObject obj in list) {
            DeactivateObject(obj);
            PauseSpriteAnim(obj);
        }
    }


    private void ResumeSurroundingObjects(List<GameObject> list)
    {
        foreach (GameObject obj in list) {
            if (obj == null) continue;
            ReactivateObject(obj);
            PauseSpriteAnim(obj);
        }
    }


    private List<GameObject> GetSurroundingMovingObjects()
    {
        List<GameObject> list = new List<GameObject>();

        GameObject[] enemiesList = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] itemsList = GameObject.FindGameObjectsWithTag("Item");
        GameObject[] powerUpsList = GameObject.FindGameObjectsWithTag("PowerUp");
        GameObject[] blocksList = GameObject.FindGameObjectsWithTag("Block");

        foreach (GameObject obj in enemiesList) list.Add(obj);
        foreach (GameObject obj in itemsList) list.Add(obj);
        foreach (GameObject obj in powerUpsList)  list.Add(obj);
        foreach (GameObject obj in blocksList)  list.Add(obj);

        return list;
    }

    private void DeactivateObject(GameObject obj)
    {
        Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
        if (rb == null) return;
        rb.simulated = false;
    }

    private void ReactivateObject(GameObject obj)
    {
        Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
        if (rb == null) return;
        rb.simulated = true;
    }

    private void PauseSpriteAnim(GameObject obj, bool paused = true)
    {
        InteractiveObject io = obj.GetComponent<InteractiveObject>();
        
        if (io == null) return;
        if (io.sprite == null) return;
        
        Animation ioAnimation = io.sprite.GetComponent<Animation>();

        if (ioAnimation == null) return;

        ioAnimation.enabled = !paused;
    }
    

    void EnableFireball()
    {
        if (fireballObj == null) return;
        fireballObj.SetActive(playerStats.currentPowerUp.sprite == SpriteName.Fire);
    }
}