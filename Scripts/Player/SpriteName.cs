public enum SpriteName {
    Initial,
    Small, 
    Grown, 
    Fire,
    Dead,
    Invincible,
    TemporalImmunity
};