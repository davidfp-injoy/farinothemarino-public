﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int initialLives = 10;

    [HideInInspector]
    public int lives = 0;

    [HideInInspector]
    public int score = 0;

    [HideInInspector]
    public int coins = 0;

    [HideInInspector]
    public bool isOnGoalFlag = false;

    [HideInInspector]
    public PowerUp currentPowerUp;

    [HideInInspector]
    public PowerUp savedPowerUp;

    [HideInInspector]
    public bool isGrown = false;


}
