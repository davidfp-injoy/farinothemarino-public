using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadTurtle : Enemy
{
    [Header("Bad Turtle")]
    public bool isShelled = false;
    private int initialDamageOnContact;

    public override void Awake()
    {
        base.Awake();
        initialDamageOnContact = damageOnContact;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);

        string tag = collision.gameObject.tag;

        if (tag == "WorldBottom")
            dispatchType = DispatchType.Destroy;

        if (tag == "Lower Damage Dealer") {
            IOState playerIOState = playerObject.GetComponent<IOState>();
            if (playerIOState.isGrounded)
                MoveInShell();
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        string tag = collider.gameObject.tag;

        if (tag == "Lower Damage Dealer")
            GetSmashed();
    }

    public override void GetSmashed()
    {
        damageOnContact = 0;

        if (isShelled) {
            if (iostate.isMoving)
                movement.Stop();
            else
                MoveInShell();
        } else {
            isShelled = true;
            animator.SetBool("isShelled", true);
            base.GetSmashed();
        }

        BouncePlayer();
    }

    public void BouncePlayer()
    {
        Jumping playerJump = playerObject.GetComponent<Jumping>();
        IOState playerIOState = playerObject.GetComponent<IOState>();

        playerIOState.isGrounded = true;
        playerJump.Jump(true, true, false);
    }

    public void MoveInShell()
    {
        if (!isShelled) return;
        damageOnContact = initialDamageOnContact;
        iostate.isRunning = true;
        MoveToOppositePlayerPos();
    }

}
