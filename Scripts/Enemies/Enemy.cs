using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : Character
{
    [Header("Enemy properties")]
    public int damageOnContact = 1;
    public bool canBeSmashed = true;
    protected Movement movement;
    protected Jumping jump;
    protected GameObject playerObject;
    protected Player player;

    public override void Awake()
    {
        IgnoreCollisions();
        base.Awake();
        
        movement = gameObject.GetComponent<Movement>();
        jump = gameObject.GetComponent<Jumping>();

        playerObject = GameObject.Find("Player");
        player = playerObject.GetComponent<Player>();
    }

    public override void Start()
    {
        base.Start();
        if (movement != null)
            movement.GoTo("left");
    }


    public void OnCollisionStay2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;
        
        if (tag == "Block")
            DieByBlockHit(collision.gameObject);
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;

        if (tag == "Block")
            DieByBlockHit(collision.gameObject);
    }


    public virtual void GetSmashed()
    {
        GetDamage(health);
        PlaySmashedAnim();
    }


    public virtual void GetAbsoluteDamage(float dispatchDelay = 1)
    {
        dispatchType = DispatchType.Destroy;
        dispatchTime = dispatchDelay;

        PlayFlipJumpToBackAnim();
        GetDamage(health);
    }


    protected override void AdditionalGetDmgFromProjActions()
    {
        dispatchType = DispatchType.Destroy;
        dispatchTime = 1;

        PlayFlipJumpToBackAnim();
    }


    protected void PlayFlipJumpToBackAnim()
    {
        sprite.transform.localScale = new Vector2(
            sprite.transform.localScale.x,
            sprite.transform.localScale.y * -1
        );

        PlayJumpToBackAnim();

        DisableCollider();
        DisableTrigger();
    }


    protected virtual void PlaySmashedAnim()
    {
        PlayAnim("isSmashed", true);
    }


    protected virtual void PlayJumpToBackAnim()
    {
        _rigidbody.velocity = Vector2.up * 15;
        PlayAnim("isThrown", true);
        MoveToOppositePlayerPos(15);
    }


    protected void MoveToOppositePlayerPos(float speed = 0)
    {
        if (speed == 0)
            speed = iostate.isRunning ? movement.runningSpeed : movement.movementSpeed;

        string direction = playerObject.transform.position.x < transform.position.x ? "right" : "left";

        movement.GoTo(direction);

        iostate.currentSpeed = speed;
    }


    private void IgnoreCollisions()
    {
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        int itemsLayer = LayerMask.NameToLayer("Item");
        Physics2D.IgnoreLayerCollision(enemyLayer, enemyLayer);
        Physics2D.IgnoreLayerCollision(enemyLayer, itemsLayer);
    }


    private void DieByBlockHit(GameObject blockObj)
    {
        Block block = blockObj.GetComponent<Block>();

        if (block.isBeingHit)
            GetAbsoluteDamage();
    }
}
