using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadCroco : Enemy
{
    public Vector2 outPosition;
    private float maxWaitTime = 3;
    private Vector2 currentDestination;
    private float currentSpeed = 0;
    private float currentWaitTime;

    private Coroutine C_SetMovement;

    public override void Awake()
    {
        base.Awake();
        outPosition = transform.parent.Find("BadCrocoOutPosition").transform.position;
        currentWaitTime = maxWaitTime;
    }

    void FixedUpdate()
    {
        Move();
    }

    public override void GetSmashed()
    { return; }

    private void Move()
    {
        float distance = Vector2.Distance(transform.parent.position, outPosition);
        float speed = distance / 100;

        bool isIn = Vector2.Distance(transform.position, transform.parent.position) < 0.01f;
        bool isOut = Vector2.Distance(transform.position, outPosition) < 0.01f;
       
        if (isIn || isOut)  {
            if (isIn)
                currentSpeed = speed;
            else if (isOut)
                currentSpeed = -speed;
            
            currentWaitTime -= Time.deltaTime;
        }
        else
            currentWaitTime = maxWaitTime;

        if (currentWaitTime <= 0 || currentWaitTime == maxWaitTime)
            transform.Translate(new Vector2(0, currentSpeed), Space.Self);
    }
}
