using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Extender esta clase desde ObjectGroundChecker y renombrarla a "CharacterGroundChecker"
 * Renombrar ObjectGroundChecker a GroundChecker
 */

public class GroundChecker : MonoBehaviour
{
    public float radius = 0.1f;
    public bool showRadius = false;
    public LayerMask grounds;

    private GameObject obj;
    private Movement movement;
    private Rigidbody2D _rigidbody;
    private IOState iostate;
    private Jumping jump;
    private Character character;

    void FixedUpdate()
    {
        CheckGrounded();
    }

    void Awake()
    {
        obj         =  transform.parent.gameObject;
        iostate     =  obj.GetComponent<IOState>();
        movement    =  obj.GetComponent<Movement>();
        jump        =  obj.GetComponent<Jumping>();
        character   =  obj.GetComponent<Character>();
        _rigidbody  =  obj.GetComponent<Rigidbody2D>();
    }


    void OnDrawGizmos()
    {
        if (!showRadius) return;
        
        Gizmos.DrawWireSphere(transform.position, radius);
    }


    private void CheckGrounded()
    {
        iostate.isGrounded = Physics2D.OverlapCircle(
            transform.position,
            radius,
            grounds
        );

        character.PlayAnim("isGrounded", iostate.isGrounded);
        
        if (iostate.isGrounded && _rigidbody.velocity.y == 0) {
            if (jump != null) {
                jump.RebootJumpTimer();
                jump.StopJumping();
            }
    
            if (!iostate.isJumping) {
                character.PlayAnim("isJumping", false);
                character.SetGroundedCollider();
            }

            // COMENTADO PARA EVITAR ERROR DE QUE NO SE MUEVE EN LA BUILD
            // DESCOMENTAR POR SI OCURREN COMPORTAMIENTOS EXTRAÑOS Y SE SOSPECHA QUE SEA ESTO
            // _rigidbody.velocity = new Vector2(0, _rigidbody.velocity.y);
        }
    }

}