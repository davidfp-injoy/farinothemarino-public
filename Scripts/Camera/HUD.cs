﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    private GameObject scoreTextObj;
    private GameObject coinsTextObj;
    private GameObject timeTextObj;
    private GameObject testTextObj;

    public GameObject itemsTextObj;

    public Text scoreText;
    public Text coinsText;
    public Text timeText;
    public Text testText;


    void Awake()
    {
        scoreTextObj = GameObject.Find("uiScoreText");
        coinsTextObj = GameObject.Find("uiCoinsText");
        timeTextObj = GameObject.Find("uiTimeText");
        testTextObj = GameObject.Find("TEST_TEXT");

        if (scoreTextObj)
            scoreText = scoreTextObj.GetComponent<Text>();

        if (coinsTextObj)
            coinsText = coinsTextObj.GetComponent<Text>();

        if (timeTextObj)
            timeText = timeTextObj.GetComponent<Text>();

        if (testTextObj)
            testText = testTextObj.GetComponent<Text>();
    }

    public void UpdateScore(string text)
    {
        if (scoreText == null) return;
        scoreText.text = text;
    }

    public void UpdateCoins(string text)
    {
        if (coinsText == null) return;
        coinsText.text = text;
    }

    public void UpdateTime(string text)
    {
        if (timeText == null) return;
        timeText.text = text;
    }

    public void UpdateTestText(string text)
    {
        if (testText == null) return;
        testText.text = text;
    }
}
