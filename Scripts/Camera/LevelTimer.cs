﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTimer : MonoBehaviour
{
    public int time = 300;

    public GameObject playerObject;

    protected HUD hud;

    Coroutine C_RunTime;
    

    void Awake()
    {
        hud = gameObject.GetComponent<HUD>();
        ShowTime();

        Player player = playerObject.GetComponent<Player>();
    }

    void Start()
    {
        C_RunTime = StartCoroutine("RunTime");
    }

    IEnumerator RunTime()
    {
        while (time > 0) {
            yield return new WaitForSeconds(1);
            time--;
            ShowTime();
        }
    }

    void ShowTime()
    {
        if (hud == null) return;
        string currentTime = time.ToString();
        hud.UpdateTime(currentTime);
    }
}
