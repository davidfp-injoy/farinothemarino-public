using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSetter : MonoBehaviour
{
    private AudioManager bgmMan;

    void Awake()
    {
        bgmMan = GetComponent<AudioManager>();
        if (bgmMan == null) return;
        bgmMan.Play("Main OST");
    }
}
