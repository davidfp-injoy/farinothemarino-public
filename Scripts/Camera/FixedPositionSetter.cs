﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedPositionSetter : MonoBehaviour
{
    private bool onFixedPosition = false;

    private CameraPosition cameraPosition;

    void Awake()
    {
        cameraPosition = GetComponent<CameraPosition>();
    }


    public void FixCameraOnPosition(GameObject fixedPositionSetter)
    {
        SetOnFixedPosition();
        gameObject.transform.position = fixedPositionSetter.transform.position;
    }

    
    public void SetOnFixedPosition(bool confirm = true)
    {
        onFixedPosition = confirm;
        cameraPosition.enabled = !confirm;
    }
}
