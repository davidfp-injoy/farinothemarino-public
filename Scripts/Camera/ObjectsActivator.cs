using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsActivator : MonoBehaviour
{
    public float minDistance = 15;

    private List<GameObject> inactiveList = new List<GameObject>();


    void Start()
    {
        GetInactiveList();
    }


    void FixedUpdate()
    {
        for (int i = 0; i < inactiveList.Count; i++) {
            GameObject obj = inactiveList[i];

            if (obj == null) continue;

            float distance = Vector2.Distance(
                transform.position,
                obj.transform.position
            );

            obj.SetActive(distance <= minDistance);

            // if (distance > minDistance) continue;
            // obj.SetActive(true);
        }
    }


    void GetInactiveList() {
        GameObject[] _raw = GameObject.FindObjectsOfType<GameObject>();
        
        for (int i = 0; i < _raw.Length; i++) {
            GameObject obj = _raw[i];
            InteractiveObject component = obj.GetComponent<InteractiveObject>();

            if (component == null) continue;

            if (!component.startInactive) continue;

            inactiveList.Add(obj);

            obj.SetActive(false);
        }
    }
}
