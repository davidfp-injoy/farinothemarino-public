﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    public GameObject player;

    private bool canMove = true;

    Camera _camera;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(transform.position.x, 8, -200);
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveWithPlayer();
    }


    void MoveWithPlayer()
    {
        if (!canMove || player == null) return;

        float x = player.transform.position.x;

        // if (_camera.WorldToScreenPoint(player.transform.position).x < (_camera.pixelWidth / 2))
        //     x = transform.position.x;

        transform.position = new Vector3(x, 8, -200);
    }
}
