﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IOState))]
public class Character : DestructibleObject
{
    protected bool canControl = true;

    public SizeType initialSize = SizeType.Small;

    [Header("Colliders and Triggers")]
    public bool showCollidersGizmos = true;


    [Header("Collider Properties")]
    public CharacterColliderPropsStruct smallColliderProps;
    public CharacterColliderPropsStruct grownColliderProps;


    [Header("Trigger Properties")]
    public CharacterColliderPropsStruct smallTriggerProps;
    public CharacterColliderPropsStruct grownTriggerProps;

    [HideInInspector]
    public CapsuleCollider2D mainTrigger;

    [Header("Damage Dealer")]
    public GameObject[] damageDealers;


    public override void Awake()
    {
        base.Awake();
        mainTrigger = gameObject.GetComponent<CapsuleCollider2D>();
        SetInitialSize();

        // if (sprite.transform.localScale.x == -1)
        //     iostate.isLookingAt = "left";
            
        // else if (sprite.transform.localScale.x == 1)
        //     iostate.isLookingAt = "right";
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        string tag = collision.gameObject.tag;

        if (tag == "WorldBottom") {
            _rigidbody.simulated = false;
            GetDamage(health);
        }
    }


    public void StopControls(bool stopControls = true)
    {
        canControl = !stopControls;
    }

    public bool CanControl()
    {
        return canControl;
    }


    public void TravelOnPipe(GameObject pipeObject, bool conditionsMet)
    {
        Pipe pipe = pipeObject.GetComponent<Pipe>();

        if (!pipe) return;

        pipe.Travel(gameObject, conditionsMet);
    }


    public void SetDamageDealerStatus(string name, bool isEnabled = true) {
        for (int i = 0; i < damageDealers.Length; i++) {
            GameObject _obj = damageDealers[i];
            DamageDealer dd = _obj.GetComponent<DamageDealer>();

            if (dd._name != name) continue;

            _obj.SetActive(isEnabled);
            break;
        }
    }


    public void SetInitialSize()
    {
        iostate.isGrown = initialSize == SizeType.Grown;
        SetGroundedCollider();
    }


    // SET COLLIDER AND TRIGGER PROPERTIES

    public void SetColliderSize()
    {
        if (iostate.isGrounded)
            SetGroundedCollider();
        else
            SetJumpingCollider();
    }

    public void SetGroundedCollider()
    {
        if (iostate.isGrown)
            SetGrownGroundedCollider();
        else
            SetSmallGroundedCollider();

        SetGroundedTrigger();
    }


    public void SetSmallGroundedCollider()
    {
        mainCollider.offset = smallColliderProps.grounded.offset;
        (mainCollider as BoxCollider2D).size = smallColliderProps.grounded.size;
    }


    public void SetGrownGroundedCollider()
    {
        mainCollider.offset = grownColliderProps.grounded.offset;
        (mainCollider as BoxCollider2D).size = grownColliderProps.grounded.size;

        // SI CAUSA PROBLEMAS EL GROUND CHECKER EN CASOS MUY PUNTUALES
        // SE PUEDE AJUSTAR EN ESTA FUNCIÓN
    }


    public void SetJumpingCollider()
    {
        if (iostate.isGrown)
            SetGrownJumpingCollider();
        else
            SetSmallJumpingCollider();

        SetJumpingTrigger();
    }


    protected void SetSmallJumpingCollider()
    {
        mainCollider.offset = smallColliderProps.jumping.offset;
        (mainCollider as BoxCollider2D).size = smallColliderProps.jumping.size;
    }


    protected void SetGrownJumpingCollider()
    {
        mainCollider.offset = grownColliderProps.jumping.offset;
        (mainCollider as BoxCollider2D).size = grownColliderProps.jumping.size;
    }


    // SET COLLIDER AND TRIGGER PROPERTIES
    public void SetTriggerSize()
    {
        if (mainTrigger == null) return;

        if (iostate.isGrounded)
            SetGroundedTrigger();
        else
            SetJumpingTrigger();

    }

    protected void SetGroundedTrigger()
    {
        if (mainTrigger == null) return;

        if (iostate.isGrown)
            SetGrownGroundedTrigger();
        else
            SetSmallGroundedTrigger();
    }


    public void SetSmallGroundedTrigger()
    {
        if (mainTrigger == null) return;

        mainTrigger.offset = smallTriggerProps.grounded.offset;
        (mainTrigger as CapsuleCollider2D).size = smallTriggerProps.grounded.size;
    }


    public void SetGrownGroundedTrigger()
    {
        if (mainTrigger == null) return;

        mainTrigger.offset = grownTriggerProps.grounded.offset;
        (mainTrigger as CapsuleCollider2D).size = grownTriggerProps.grounded.size;
    }


    protected void SetJumpingTrigger()
    {
        if (mainTrigger == null) return;

        if (iostate.isGrown)
            SetGrownJumpingTrigger();
        else
            SetSmallJumpingTrigger();
    }


    public void SetSmallJumpingTrigger()
    {
        if (mainTrigger == null) return;

        mainTrigger.offset = smallTriggerProps.jumping.offset;
        (mainTrigger as CapsuleCollider2D).size = smallTriggerProps.jumping.size;
    }


    public void SetGrownJumpingTrigger()
    {
        if (mainTrigger == null) return;

        mainTrigger.offset = grownTriggerProps.jumping.offset;
        (mainTrigger as CapsuleCollider2D).size = grownTriggerProps.jumping.size;
    }


    public void DisableTrigger(bool disable = true)
    {
        mainTrigger.enabled = !disable;
    }
    

    private void OnDrawGizmos()
    {
        if (!showCollidersGizmos) return;

        IOState _iostate = gameObject.GetComponent<IOState>();

        if (_iostate.isJumping) {
            if (!_iostate.isGrown && smallColliderProps.jumping.showGizmo)
                DrawColliderPropsGizmo(smallColliderProps.jumping);

            else if (_iostate.isGrown && grownColliderProps.jumping.showGizmo)
                DrawColliderPropsGizmo(grownColliderProps.jumping);

            if (!_iostate.isGrown && smallTriggerProps.jumping.showGizmo)
                DrawColliderPropsGizmo(smallTriggerProps.jumping);

            else if (_iostate.isGrown && grownTriggerProps.jumping.showGizmo)
                DrawColliderPropsGizmo(grownTriggerProps.jumping);
        }
        else {
            if (!_iostate.isGrown && smallColliderProps.grounded.showGizmo)
                DrawColliderPropsGizmo(smallColliderProps.grounded);

            else if (_iostate.isGrown && grownColliderProps.grounded.showGizmo)
                DrawColliderPropsGizmo(grownColliderProps.grounded);

            if (!_iostate.isGrown && smallTriggerProps.grounded.showGizmo)
                DrawColliderPropsGizmo(smallTriggerProps.grounded);

            else if (_iostate.isGrown && grownTriggerProps.grounded.showGizmo)
                DrawColliderPropsGizmo(grownTriggerProps.grounded);
        }
    }

    void DrawColliderPropsGizmo(ColliderBasePropertiesStruct colliderProps) {
        Gizmos.DrawWireCube(
            new Vector3(
                gameObject.transform.position.x + colliderProps.offset.x,
                gameObject.transform.position.y + colliderProps.offset.y,
                0
            ),
            colliderProps.size
        );
    }

}
