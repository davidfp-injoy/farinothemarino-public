using UnityEngine;

[System.Serializable]

public struct CharacterColliderPropsStruct {
    public ColliderBasePropertiesStruct grounded;
    public ColliderBasePropertiesStruct jumping;
}