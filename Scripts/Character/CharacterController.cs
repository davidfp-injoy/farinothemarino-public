﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterController : MonoBehaviour
{
    protected Movement movement;
    protected Jumping jump;
    protected IOState iostate;
    protected Character character;

    protected virtual void Awake()
    {
        movement   =  gameObject.GetComponent<Movement>();
        jump       =  gameObject.GetComponent<Jumping>();
        iostate    =  gameObject.GetComponent<IOState>();
        character  =  gameObject.GetComponent<Character>();
    }

    protected virtual void Update()
    {
        if (!character.CanControl()) return;

        Move();
        Run();
        Jump();
        DoPowerUpAction();
    }

    
    protected abstract void Move();

    protected abstract void Run();

    protected abstract void Jump();

    protected virtual void DoPowerUpAction() {
        
    }

}
